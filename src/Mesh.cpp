#include "Mesh.h"
string toString(double source)
{
	char buf[100];
	sprintf(buf, "%lf", source);
	string s = buf;
	return s;
}

string toString(int source)
{
	char buf[100];
	sprintf(buf, "%d", source);
	string s = buf;
	return s;
}

Mesh::Mesh(const char *filename, int format)
{
	// rozpoznanie formatu i wczytanie kształtu
	if (format == SHP)
		importShp(filename);
	else if (format == OBJ)
		importObj(filename);
}

void Mesh::importShp(const char *filename)
{
	double x,y,z;
	FILE *f = fopen(filename, "r");
	if (f == NULL)
	{
		perror("Could not open file...");
		exit(0);
	}

	int num_indices, num_vertices;

	fscanf(f, "%d %d", &num_vertices, &num_indices);
	//printf("%d %d\n", num_vertices, num_indices);
	// 	float vertices[num_vertices][3];
	// 	int indices[num_indices][3];
	for (int i = 0; i < num_vertices; i++) {
		fscanf(f, "%lf %lf %lf ", &x,&y,&z);
		vertices.push_back( dvec3(x,y,z) );
		tex_coords.push_back( dvec2(0.) );
		normals.push_back( dvec3(1.) );
	}
	for (int i = 0; i < num_indices; i++) {
		Face face;
		fscanf(f, "%d %d %d ", &face.A, &face.B, &face.C);
		// od razu przejscie z 1 do 0 based indeksów
		face.A--;
		face.B--;
		face.C--;
		faces.push_back(face);
	}
	fclose(f);
}

void Mesh::importObj(const char* filename)
{
	FILE *f = fopen(filename, "r");
	if (f == NULL)
	{
		perror("Could not open file...");
		exit(0);
	}

	// zmienne pomocnicze, bufory
	char line[1000], buff[100], ch;
	double vtx, vty, vx, vy, vz;
	char f1[100], f2[100], f3[100], fvt1[100], fvt2[100], fvt3[100];

	vector<string> header_text;
	// zczytanie komentarzy
	// 		FILE *f = fopen(filename, "r");
	while (fgets(line, 1000, f) != 0)
	{
		switch (line[0])
		{
			case '#':
				if ( line[1] != '!')
				{
					string l = line;
					header_text.push_back(l);
				}
				break;
			case 'v':
				if (line[1] == 't')
				{
					sscanf(line, "%s %lf %lf", buff, &vtx, &vty);
					tex_coords.push_back(dvec2(vtx, vty) );
					// 						has_tex_coords = true;
				}
				else
				{
					sscanf(line, "%s %lf %lf %lf", buff, &vx, &vy, &vz);
					vertices.push_back( dvec3(vx, vy, vz) );
				}
				break;
			case 'f':
				string l = line;
				int c = count(l.begin(), l.end(), '/');
				if (c == 0)
				{
					Face face;
					sscanf(line,"%s %d %d %d",buff,&face.A,&face.B,&face.C);
					face.A--; face.B--; face.C--;
					faces.push_back(face);
				}
				else if (c == 3)
				{
					Face face;
					string l = line;
					sscanf(line,"%s %[^/]/%s %[^/]/%s %[^/]/%s" ,
							buff, f1, fvt1, f2, fvt2, f3, fvt3);
					face.A = atoi(f1)-1; face.tA = atoi(fvt1)-1;
					face.B = atoi(f2)-1; face.tB = atoi(fvt2)-1;
					face.C = atoi(f3)-1; face.tC = atoi(fvt2)-1;
					// -1 !!!!!!!!!!!!!!!!!!

					faces.push_back(face);
				}
				break;

		}
	}
	fclose(f);
	// usunac konce lini w tekscie
	for (vector<string>::iterator it = header_text.begin();
			it != header_text.end(); it++)
	{
		size_t pos = it->find("\n");
		if (pos != string::npos)
			it->erase(pos);
	}

	// 		for (int i = 0; i < header_text.size(); i++)
	// 			cout << header_text[i] << "\n";

	header.merge(header_text);
}

double Mesh::findMaxVertexLength()
{
	/// length of the longest vector in vertices
	double max_r = -1e6;
	double r;
	for (int i = 0; i < vertices.size(); i++)
	{
		r = length(vertices[i]);
		if (r > max_r)
			max_r = r;
	}
	return max_r;
}

void Mesh::normalize()
{
	double max_r = findMaxVertexLength();

	//normalize all vertices
	for (int i = 0; i < vertices.size(); i++)
		vertices[i] /= max_r;

// 	for (int i = 0; i < vertices.size(); i++)
// 		vertices[i] /= length(vertices[i]);

	// update header
	header.merge("R_max: 1.0");
}

void Mesh::save(const char *filename, int format)
{
	if (format == SHP)
		saveShp(filename);
	else if (format == OBJ)
		saveObj(filename);
}

void Mesh::saveShp(const char *filename)
{

	FILE *f = fopen(filename, "w");
	if (f == NULL)
		cout << "cannot create an output file\n";
	fprintf(f, "%lu %lu\n", vertices.size(), faces.size());
	for (int i = 0; i < vertices.size(); i++)
		fprintf(f, "%.12lf %.12lf %.12lf\n",
				vertices[i].x, vertices[i].y, vertices[i].z);

	for (int i = 0; i < faces.size(); i++)
		fprintf(f, "%d %d %d\n",
				faces[i].A +1,
				faces[i].B +1,
				faces[i].C +1 );

	fclose(f);
}

void Mesh::saveObj(const char *filename)
{

	FILE *f = fopen(filename, "w");
	if (f == NULL)
		cout << "cannot create an output file\n";

	// add num_points and num_vertices to header
	vector<string> info;
	info.push_back("number_of_vertices: " + toString((int)vertices.size()) );
	info.push_back("number_of_facets: " + toString((int)faces.size()) );
	header.merge(info);



	// add header to file
	string header_text = header.getHeaderString();
	fprintf(f, "%s\n", header_text.c_str());

	// add vertices to file
	for (int i = 0; i < vertices.size(); i++)
		fprintf(f, "v %.12lf %.12lf %.12lf\n",
				vertices[i].x, vertices[i].y, vertices[i].z);

	if (print_tex_coords)
	{
		for (int i = 0; i < tex_coords.size(); i++)
			fprintf(f, "vt %.12lf %.12lf\n",
					tex_coords[i].x, tex_coords[i].y);

		for (int i = 0; i < faces.size(); i++)
			fprintf(f, "f %d/%d %d/%d %d/%d\n",
					faces[i].A +1, faces[i].tA +1,
					faces[i].B +1, faces[i].tB +1,
					faces[i].C +1, faces[i].tC +1);
	}
	else
	{
		for (int i = 0; i < faces.size(); i++)
			fprintf(f, "f %d %d %d\n",
					faces[i].A +1,
					faces[i].B +1,
					faces[i].C +1);

	}

	fclose(f);
}

void Mesh::print(int format)
{
	if (format == OBJ)
	{

		// print header
		string header_text = header.getHeaderString();
		printf("%s\n", header_text.c_str());


		for (int i = 0; i < vertices.size(); i++)
			printf("v %.12lf %.12lf %.12lf\n",
				  	vertices[i].x, vertices[i].y, vertices[i].z);

		if (print_tex_coords)
		{
			for (int i = 0; i < tex_coords.size(); i++)
				printf("vt %.12lf %.12lf\n",
					   	tex_coords[i].x, tex_coords[i].y);

			for (int i = 0; i < faces.size(); i++)
				printf("f %d/%d %d/%d %d/%d\n",
						faces[i].A +1, faces[i].tA +1,
						faces[i].B +1, faces[i].tB +1,
						faces[i].C +1, faces[i].tC +1);
		}
		else
		{
			for (int i = 0; i < faces.size(); i++)
				printf("f %d %d %d\n",
						faces[i].A +1,
						faces[i].B +1,
						faces[i].C +1);

		}
	}

	if (format == SHP)
	{
		printf("%lu %lu\n", vertices.size(), faces.size());
		for (int i = 0; i < vertices.size(); i++)
			printf("%.12lf %.12lf %.12lf\n",
				  	vertices[i].x, vertices[i].y, vertices[i].z);

		for (int i = 0; i < faces.size(); i++)
			printf("%d %d %d\n",
				   	faces[i].A +1,
				   	faces[i].B +1,
				   	faces[i].C +1 );
	}

}

void Mesh::calculateTextureCoordinates()
{

	for (int i =0; i < triangles.size(); i++)
	{
		for (int j = 0; j < 3; j++) {
			dvec3 point = triangles[i].vertices[j];

			double r = length(point);
			double alpha = atan2(point.y, point.x);
			double beta = acos(point.z / r);
			//
			// alpha (azymut) do zakresu [0,2pi)
			if (alpha < 0)
				alpha += 2*M_PI;

			// do zakresu [0,1)
			alpha /= 2*M_PI;
			beta = 1. - beta/M_PI;

			triangles[i].tex_coords[j].x = alpha;
			triangles[i].tex_coords[j].y = beta;
		}
	}

}

void Mesh::calculateTrianglesBeta()
{

	for (int i =0; i < triangles.size(); i++)
	{
		triangles[i].calculateBeta();
	}

}


void Mesh::removeInside(int GRID_DIM)
{
	int count = 0;

	dvec3 center(GRID_DIM/2); // center point
	dvec3 vec;

	for (int i = 0; i < triangles.size(); i++)
	{
		triangles[i].calculateNormal();

		vec = triangles[i].vertices[0] - center; // wektor w reprezentacji od środka gridu

		if ( dot( triangles[i].normal, vec) < -0.02)
		{
			triangles.erase(triangles.begin() + i);
// 			cout << "triangle "<<i<<" removed" << "\n";
			count++;
			i--;
		}
	}
	cout << count <<" triangles removed" << "\n";
}

void Mesh::center(int GRID_DIM)
{

	for (int i = 0; i < triangles.size(); i++)
		for (int j = 0; j < 3; j++)
			triangles[i].vertices[j] -= dvec3(GRID_DIM/2);
}

void Mesh::arraysToTriangles()
{
	triangles.clear();
	Triangle triangle;

	for (int i = 0; i < faces.size(); i++)
	{
		triangle.vertices[0].x = vertices[ faces[i].A ].x;
        triangle.vertices[0].y = vertices[ faces[i].A ].y;
        triangle.vertices[0].z = vertices[ faces[i].A ].z;

        triangle.vertices[1].x = vertices[ faces[i].B ].x;
        triangle.vertices[1].y = vertices[ faces[i].B ].y;
        triangle.vertices[1].z = vertices[ faces[i].B ].z;

        triangle.vertices[2].x = vertices[ faces[i].C ].x;
        triangle.vertices[2].y = vertices[ faces[i].C ].y;
        triangle.vertices[2].z = vertices[ faces[i].C ].z;

// 		if (arrays.has_tex_coords)
// 		{
// 			triangle.tex_coords[0].x= arrays.tex_coords[ arrays.faces[i].tA ].x;
// 			triangle.tex_coords[0].y= arrays.tex_coords[ arrays.faces[i].tA ].y;
//
// 			triangle.tex_coords[1].x= arrays.tex_coords[ arrays.faces[i].tB ].x;
// 			triangle.tex_coords[1].y= arrays.tex_coords[ arrays.faces[i].tB ].y;
//
// 			triangle.tex_coords[2].x= arrays.tex_coords[ arrays.faces[i].tC ].x;
// 			triangle.tex_coords[2].y= arrays.tex_coords[ arrays.faces[i].tC ].y;
// 		}
//
// 		if (arrays.has_normals)
// 			triangle.normal = arrays.normals[ arrays.faces[i].n ];

		triangles.push_back(triangle);
	}

}

void Mesh::trianglesToArrays()
{
	vertices.clear();
	normals.clear();
	tex_coords.clear();
	faces.clear();

	int index;
	ivec3 v, tex;
	Face face;
	for (int i = 0; i < triangles.size(); i++)
	{
		for (int j = 0; j < 3; j++)
		{
			//szuka, czy punkt jest w tablicy w array, jak nie to go dopisuje
			vector<dvec3>::iterator it =
				find(vertices.begin(), vertices.end(),
						triangles[i].vertices[j]);

			if (it == vertices.end() )	// nie ma go
			{
				vertices.push_back( triangles[i].vertices[j] );
				index = vertices.size() - 1;
				v[j] = index;
			}
			else
			{
				index = std::distance( vertices.begin(), it);
				v[j] = index;
			}

			// dla texturki
			vector<dvec2>::iterator it2 =
				find(tex_coords.begin(), tex_coords.end(),
				   	triangles[i].tex_coords[j] );

			if (it2 == tex_coords.end() )
			{
				tex_coords.push_back( triangles[i].tex_coords[j] );
				index = tex_coords.size() - 1;
				tex[j] = index;
			}
			else
			{
				index = std::distance( tex_coords.begin(), it2);
				tex[j] = index;
			}

		}

		face.A = v.x; face.B = v.y; face.C = v.z;
		face.tA = tex.x; face.tB = tex.y; face.tC = tex.z;
		faces.push_back(face);

	}

// 	cout  << "\n";
// 	cout << "num points: "<< vertices.size() << "\n";
// 	cout << "num faces: "<< faces.size() << "\n";

}

// void Mesh::stitch()
// {
// 	int count = 0;
// 	Triangle face, new_face1, new_face2, new_face3;
//
// 	int limit = triangles.size();
// 	for (int i = 0; i < limit; i++)
// 	{
// 		if (triangles[i].checkForPrimeMeridian() )
// 		{
// 			/* tutaj konstrukcja noweych trójkątów: */
//
// 			face = triangles[i];
//
// 			// obliczamy parametry xi i eta
// 			double xi = face.calculateXi();
// 			double eta = face.calculateEta();
//
// 			int loner_id = face.loner_id;
// // 			cout << loner_id << "\t" << xi << "\t" << eta <<"\n";
//
//
//
//
// 			// zrobić punkty A' i A"
// 			dvec3 A_prim;
// 			A_prim.x = face.vertices[loner_id].x + face.b.x * xi;
// 			A_prim.y = face.vertices[loner_id].y + face.b.y * xi;
// 			A_prim.z = face.vertices[loner_id].z + face.b.z * xi;
//
// 			dvec3 A_bis;
// 			A_bis.x = face.vertices[loner_id].x + face.c.x * eta;
// 			A_bis.y = face.vertices[loner_id].y + face.c.y * eta;
// 			A_bis.z = face.vertices[loner_id].z + face.c.z * eta;
//
//
// 			/* konstrukcja trzech nowych trójkątów */
// 			// A, A', A"
// 			new_face1.vertices[0] = face.vertices[loner_id];
// 			new_face1.vertices[1] = A_prim;
// 			new_face1.vertices[2] = A_bis;
//
//
// 			// A', B, C
// 			new_face2.vertices[0] = A_prim;
// 			new_face2.vertices[1] = face.vertices[(loner_id+1) % 3];
// 			new_face2.vertices[2] = face.vertices[(loner_id+2) % 3];
//
//
// 			// A', C, A"
// 			new_face3.vertices[0] = A_prim;
// 			new_face3.vertices[1] = face.vertices[(loner_id+2) % 3];
// 			new_face3.vertices[2] = A_bis;
// 			/*****************************************/
//
//
// 			if ( Triangle::isNearOne(face.tex_coords[loner_id].x ))
// 			{
// 				/** znaczy to, że looner jest z lewej, czyli A, A' A"
// 				 * mają mieć alphe = 1,
// 				 * reszta alphe = 0 bo są przy zerze z prawej
// 				 *
// 				 * 			|						|
// 				 * 			|	B				B	|
// 				 * 		A	|						|	A
// 				 * 			|						|
// 				 * 			|	C				C	|
// 				 * 			|						|
// 				 *
// 				 */
//
// 				new_face1.tex_coords[0].x = face.tex_coords[loner_id].x;
// 				new_face1.tex_coords[1].x = 1.0;
// 				new_face1.tex_coords[2].x = 1.0;
//
// 				new_face2.tex_coords[0].x = 0.0;
// 				new_face2.tex_coords[1].x = face.tex_coords[(loner_id+1) % 3].x;
// 				new_face2.tex_coords[2].x = face.tex_coords[(loner_id+2) % 3].x;
//
// 				new_face3.tex_coords[0].x = 0.0;
// 				new_face3.tex_coords[1].x = face.tex_coords[(loner_id+2) % 3].x;
// 				new_face3.tex_coords[2].x = 0.0;
// 			}
// 			if ( Triangle::isNearZero(face.tex_coords[loner_id].x ))
// 			{
// 				/** jak wyżej, tylko że odwrotnie :) */
//
// 				new_face1.tex_coords[0].x = face.tex_coords[loner_id].x;
// 				new_face1.tex_coords[1].x = 0.0;
// 				new_face1.tex_coords[2].x = 0.0;
//
// 				new_face2.tex_coords[0].x = 1.0;
// 				new_face2.tex_coords[1].x = face.tex_coords[(loner_id+1) % 3].x;
// 				new_face2.tex_coords[2].x = face.tex_coords[(loner_id+2) % 3].x;
//
// 				new_face3.tex_coords[0].x = 1.0;
// 				new_face3.tex_coords[1].x = face.tex_coords[(loner_id+2) % 3].x;
// 				new_face3.tex_coords[2].x = 1.0;
// 			}
//
//
//
// 			triangles.push_back(new_face1);
// 			triangles.push_back(new_face2);
// 			triangles.push_back(new_face3);
//
// 			/****************************************/
//
// 			// usunięcie marudera
// 			count ++;
// 			triangles.erase( triangles.begin() + i);
// 			i--;
// 			limit--;
// 		}
//
// 	}
//
// 	cout << count << " triangles modified\n";
// 	calculateTrianglesBeta(); 				// przeliczyć bete dla wszystkich
//
//
// 	header.merge("stitched: true");
//
// 	print_tex_coords = true; 	// nie ma sensu tego robić inaczej
// }
