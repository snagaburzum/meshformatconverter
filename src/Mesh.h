#ifndef MESH_H_
#define MESH_H_

#include <iostream>
#include <stdio.h>
#include <vector>
#include <string.h>
#include <algorithm>
#include <fstream>
#include <sstream>
#include <map>
#include "Triangle.h"
#include "Header.h"

using namespace std;
using namespace glm;



class Face
{
public:
	Face() {A=B=C=tA=tB=tC=n= -1;}
	int A, B, C; 	// indeksy punktów
	int tA, tB, tC; // indeksy wsp. tekstury dla punktów A B C
	int n; 			// indeks normalnej do powierzchni
};

class Mesh
{
public:
	Mesh(const char* filename, int format);
	void normalize();
	void save(const char* filename, int format);
	void print(int format);

	void arraysToTriangles();
	void trianglesToArrays();

	void calculateTextureCoordinates();
	void calculateTrianglesBeta();
// 	void stitch();
	void removeInside(int GRID_DIM);
	void center(int GRID_DIM);

private:
	void importShp(const char* filename);
	void importObj(const char* filename);
	void saveShp(const char* filename);
	void saveObj(const char* filename);

	double findMaxVertexLength();


public:
	enum {
		SHP,
		OBJ,
		TERRAIN
	};

	Header header;

	bool print_tex_coords;

	vector<dvec3> vertices;
	vector<dvec3> normals;
	vector<dvec2> tex_coords;
	vector<Face> faces;

	bool has_tex_coords, has_normals;
// 	vector<string> comments;


	vector<Triangle> triangles;
};



#endif /* MESH_H_ */
