#ifndef HEADER_H_
#define HEADER_H_

#include <iostream>
#include <stdio.h>
#include <vector>
#include <string>
#include <map>
#include <fstream>
#include <sstream>

using namespace std;

class Header {
public:
	Header();
	void loadInpFile(string inp_filename);
	void loadSpinFile(string spin_filename);
	void randomInfo(string, string, string);
	void merge(vector<string>);
	void merge(string); /// inefficient, should be avoided
	string getHeaderString();
	const string currentDateTime();

	vector<string> keys, values, text;//, input_text;
	map<string,string> input_pairs;
};
#endif /* HEADER_H_ */
