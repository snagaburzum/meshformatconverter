#ifndef TRIANGLE_H
#define TRIANGLE_H

#include <vector>
#include <iostream>
#include "glm/glm.hpp"

using namespace std;
using namespace glm;



class Triangle
{
public:
	dvec2 tex_coords[3];
	dvec3 vertices[3];
	dvec3 normal;

	int loner_id;	// liczony jest w funkji checkForPrimeMeridian !!
	bool contains_prime_meridian;

	dvec3 b, c;
	double xi, eta;

	/* copy constructor
	 * classname (const classname &obj) {
	 *  // body of constructor
	 *	}
	 */
	Triangle (const Triangle &obj);

	Triangle();

	bool checkForPrimeMeridian();
	double calculateXi();
	double calculateEta();
	void calculateBeta();
	void calculateNormal();

	static bool isNearZero(double u);
	static bool isNearOne(double u);

};

#endif // TRIANGLE_H
