#include "Options.h"

Options::Options()
{
	// initialize members
	out_format = 0;
	normalize_mesh = false;
   	print_tex_coords = false;
	remove_inside = false;
   	center = false;
	load_inp = false;
	load_spin = false;
	random_info = false;
}

Options::Options(int argc, char *argv[])
{
	// initialize members
	out_format = 0;
	normalize_mesh = false;
   	print_tex_coords = false;
	remove_inside = false;
   	center = false;
	load_inp = false;
	load_spin = false;
	random_info = false;

	parse(argc, argv);
}

void Options::printHelp()
{
	char text[] =
"\n"
"        Author: Grzegorz Dudzinski       g.p.dudzinski@gmail.com                \n"
"                                                                                \n"
"        converts mesh files and performs operations on mesh                     \n"
"                                                                                \n"
"Usage: ./meshFormatConverter input_filename output_filename [options]           \n"
"                                                                                \n"
" IF no file formats are specified, file extensions are parsed                   \n"
"                                                                                \n"
"\nOptions:                                                                      \n"
"-if, --input_format <format>    file format for input file:                     \n"
"                                obj - wavefront obj file                        \n"
"                                |v .. .. ..                   |                 \n"
"                                |f ../../.. ../../.. ../../.. |                 \n"
"                                                                                \n"
"                                shp - wavefront shape                           \n"
"                                |num_vertices num_indices     |                 \n"
"                                |... ... ...                  |                 \n"
"                                |... ... ...                  |                 \n"
"                                                                                \n"
"                                                                                \n"
"-of, --output_format <format>   see --input_format                              \n"
"                                                                                \n"
"-inp filename                   load information from .inp file to header       \n"
"-spin filename                  load information from SPIN DAMIT file to       \n"
"                                header                                          \n"
"-random_info <grid> <harmonics> <seed>      print grid subdivision depth      \n"
"                                <grid>, harmonics degree <harmonics> and        \n"
"                                random generator <seed> to the header           \n"
"                                                                                \n"
"-n, --normalize                 normalize input mesh                            \n"
"-notex                          do not print texture coordinates                \n"
"-tex                            print texture coordinates                       \n"
"-remove_inside GRID_DIM         remove triangles with angle between normal and  \n"
"                                direction to body center grater than 90deg      \n"
"                                GRID_DIM -- dimenstions used in generating the  \n"
"                                model, used for center computation              \n"
"-center GRID_DIM                move body to the center. GRID_DIM - grid size   \n"
"                                                                                \n"
"-update_header <string>         string=key:value,key2:value2,...           \n"
"                                                                                \n"
"                                                                                \n"
// "Header:                                                                         \n"
// "--name                          Asteroid's name                                 \n"
// "--author                        Author                                          \n"
// "--date                          Date in format: YYYY-MM-DD.HH:MM:SS             \n"
// "--method                        Inversion method used to make model             \n"
// "--lambda                        Pole solution                                   \n"
// "--beta                          Pole solution                                   \n"
// "--period                        Period                                          \n"
"                                                                                \n"
"                                                                                \n"
"";


	cout << text << "\n";
}

void Options::parse(int argc, char *argv[])
{

	if (argc == 1)
	{
		cout << "To few arguments. use option --help or -h for help\n";
		exit(EXIT_SUCCESS);
	}

	if (argc == 2)
		if (strcmp(argv[1], "-h") == 0 || strcmp(argv[1],"--help")==0)
		{
			printHelp();
			exit(EXIT_SUCCESS);
		}
		else
		{
			cout << "To few arguments. use option --help or -h for help\n";
			exit(EXIT_SUCCESS);
		}

	filename_in = argv[1];
    filename_out = argv[2];

	// parse file extensions
	// input file:
	int last_index = strlen(argv[1]) - 1;
	char extension[4];

	for (int i = last_index; i > 0; i--)
	{			 // jadymy od tylu
		if (argv[1][i] ==  '.' )
		{
			strncpy(extension, argv[1] +i+1, 3);
			extension[3] = 0;

			if (strcmp( extension, "shp") == 0 )
				in_format = Mesh::SHP;
			if (strcmp( extension, "obj") == 0 )
				in_format = Mesh::OBJ;
			break;
		}
	}
	// output file
	last_index = strlen(argv[2]) - 1;
	for (int i = last_index; i > 0; i--)
	{			 // jadymy od tylu
		if (argv[2][i] ==  '.' )
		{
			strncpy(extension, argv[2] +i+1, 3);
			extension[3] = 0;

			if (strcmp( extension, "shp") == 0 )
				out_format = Mesh::SHP;
			if (strcmp( extension, "obj") == 0 )
				out_format = Mesh::OBJ;
			break;
		}
	}



	for (int i = 3; i < argc; i++)
	{
		if ( strcmp(argv[i], "-of") == 0
				|| strcmp(argv[i], "--output_format") == 0)
		{
			if (strcmp(argv[i+1], "obj") == 0)
				out_format = Mesh::OBJ;
			else if (strcmp(argv[i+1], "shp") == 0)
				out_format = Mesh::SHP;
			else
			{
				cout << "output format not supported" << "\n";
				exit(EXIT_FAILURE);
			}
		}

		if ( strcmp(argv[i], "-if") == 0
				|| strcmp(argv[i], "--input_format") == 0)
		{
			if (strcmp(argv[i+1], "obj") == 0)
				in_format = Mesh::OBJ;
			else if (strcmp(argv[i+1], "shp") == 0)
				in_format = Mesh::SHP;
			else if (strcmp(argv[i+1], "ter") == 0)
				in_format = Mesh::TERRAIN;
			else
			{
				cout << "input format not supported" << "\n";
				exit(EXIT_FAILURE);
			}
		}

		if (strcmp(argv[i], "-n") == 0 || strcmp(argv[i],"--normalize")==0)
			normalize_mesh = true;

// 		if (strcmp(argv[i], "-s") == 0 || strcmp(argv[i],"--stitch")==0)
// 			stitch = true;

		if (strcmp(argv[i], "-notex") == 0 )
			print_tex_coords = false;
		if (strcmp(argv[i], "-tex") == 0 )
			print_tex_coords = true;


		if (strcmp(argv[i], "-remove_inside") == 0 )
		{
			remove_inside = true;
			GRID_DIM = atoi(argv[i+1]);
		}
		if (strcmp(argv[i], "-center") == 0 )
		{
			center = true;
			GRID_DIM = atoi(argv[i+1]);
		}

		if (strcmp(argv[i], "-inp") == 0 )
		{
			load_inp = true;
			inp_filename = argv[i+1];
		}

		if (strcmp(argv[i], "-spin") == 0 )
		{
			load_spin = true;
			spin_filename = argv[i+1];
		}


		if (strcmp(argv[i], "-random_info") == 0 )
		{
			random_info = true;
			grid =  argv[i+1];
			harmonics =  argv[i+2];
			seed =  argv[i+3];
		}

		if (strcmp(argv[i], "-update_header") == 0 )
		{
			update_header = true;
			header_raw_text = argv[i+1];
			header_items = split(header_raw_text, ',');
		}


// 		if (strcmp(argv[i], "--name") == 0 )
// 			sprintf(NAME, "%s", argv[i+1]);
// 		if (strcmp(argv[i], "--author") == 0 )
// 			sprintf(AUTHOR, "%s",  argv[i+1]);
// 		if (strcmp(argv[i], "--date") == 0 )
// 			sprintf(DATE, "%s",  argv[i+1]);
// 		if (strcmp(argv[i], "--method") == 0 )
// 			sprintf(METHOD, "%s",  argv[i+1]);
// 		if (strcmp(argv[i], "--lambda") == 0 )
// 			sprintf(LAMBDA, "%s",  argv[i+1]);
// 		if (strcmp(argv[i], "--beta") == 0 )
// 			sprintf(BETA, "%s",  argv[i+1]);
// 		if (strcmp(argv[i], "--period") == 0 )
// 			sprintf(PERIOD, "%s",  argv[i+1]);


	}

}

vector<string> Options::split(const string &text, char sep)
{
    vector<string> tokens;
    size_t start = 0, end = 0;
    while ((end = text.find(sep, start)) != string::npos) {
        if (end != start) {
          tokens.push_back(text.substr(start, end - start));
        }
        start = end + 1;
    }
    if (end != start) {
       tokens.push_back(text.substr(start));
    }
    return tokens;
}
