#include "Header.h"

Header::Header()
{
	keys.push_back("date");
	values.push_back(" ");

	keys.push_back("target");
	values.push_back(" ");

	keys.push_back("method");
	values.push_back(" ");

	keys.push_back("period[h]");
	values.push_back(" ");

	keys.push_back("lambda");
	values.push_back(" ");

	keys.push_back("beta");
	values.push_back(" ");

	keys.push_back("gamma");
	values.push_back(" ");

	keys.push_back("jd_gamma0");
	values.push_back(" ");

	keys.push_back("R_max");
	values.push_back(" ");

	keys.push_back("number_of_vertices");
	values.push_back(" ");

	keys.push_back("number_of_facets");
	values.push_back(" ");
}

void Header::merge(string source)
{
	vector<string> text_to_merge;
	text_to_merge.push_back(source);
	merge(text_to_merge);
}

void Header::merge(vector<string> input_text)
{
	/** z vectora stringów robi input_pairs, reszte text */
	input_pairs.clear();

	vector<string>::iterator line;
	for (line = input_text.begin(); line != input_text.end(); line++)
	{
		// szukam, czy jest jakiś key
		size_t semicolon_pos = line->find_first_of(":");
		if (semicolon_pos == string::npos)
			continue;

		// szuka spacji lub # od pozycji : wstecz
		size_t non_white_pos = line->find_last_of("#", semicolon_pos) +1;
		string key =
		   	line->substr(non_white_pos, (semicolon_pos - non_white_pos));

		string value = line->substr(semicolon_pos + 1);

		while (key[0] == ' ' )
		   key.erase(key.begin() );

		while (value[0] == ' ' )
		   value.erase(value.begin() );

// 		cout << key<<":"<<value << "\n";

		if (value.empty())
			input_pairs[key] = " ";
		else
			input_pairs[key] = value;

// 		cout << key << "\n";

		input_text.erase( line );
		line--;
	}

	// merge remaining text
	for (int i = 0; i < input_text.size(); i++)
		text.push_back(input_text[i]);

	// merge keys and values
	map<string, string>::iterator p;
	for (p = input_pairs.begin(); p != input_pairs.end(); p++)
	{
		bool found = false;
		int size = keys.size();
		for (int i = 0; i < size; i++)
			if ( p->first.compare(keys[i]) == 0)
			{
				values[i] = p->second;
				found = true;
			}

		if (!found)
		{
			keys.push_back(p->first);
			values.push_back(p->second);
		}
	}

// 	for (int i = 0; i < keys.size(); i++)
// 		cout << keys[i]<<";"<< values[i] << "\n";
}

void Header::loadInpFile(string inp_filename)
{
//
	ifstream f;
	f.open(inp_filename.c_str(), ios::in);
// 	ostringstream s;
//
	string line, temp_str;
	vector<string> inp_text;
//
	getline(f, line);
//  	inp_info["nr"] = line;
	temp_str = "nr: " + line;
// 	inp_text.push_back(temp_str);

	getline(f, line);
	double lambda = 0;
	sscanf(line.c_str(), "%lf", &lambda);

	while (lambda < 0.)
		lambda +=360.;
	while (lambda >= 360.)
		lambda -= 360.;

	char buf[1000];
	sprintf(buf, "%lf", lambda);

	temp_str = "lambda: ";
	temp_str += buf;
	inp_text.push_back(temp_str);

	getline(f, line);
	temp_str = "beta: " + line;
	inp_text.push_back(temp_str);

	getline(f, line);
	temp_str = "gamma: " + line ;
	inp_text.push_back(temp_str);

	getline(f, line);
	temp_str = "period[h]: " + line;
	inp_text.push_back(temp_str);

	getline(f, line);
// 	header.inp_info["chi2"] = atof(line.c_str());
	getline(f, line);
	temp_str = "jd_gamma0: " + line;
	inp_text.push_back(temp_str);

//
	f.close();
//
	cout << "inp loaded." << "\n";
//
 	merge(inp_text);
}

void Header::randomInfo(string grid, string harmonics, string seed)
{
	vector<string> info;
	info.push_back("method: RANDOM");

	info.push_back("Grid_subdivision_depth: " + grid);
	info.push_back("harmonics_degree: " + harmonics);
	info.push_back("random_seed: " + seed);

	merge(info);
}

void Header::loadSpinFile(string spin_filename)
{
	ifstream f;
	f.open(spin_filename.c_str(), ios::in);
	if (!f.is_open())
	{
		cout << "can't open the file" << "\n";
		return;
	}

	double lambda, beta, P, jd0, gamma0;

	string line, temp_str;
	char buf[1000];
	vector<string> spin_text;

	getline(f, line);
	sscanf(line.c_str(), "%lf %lf %lf", &lambda, &beta, &P);
	getline(f, line);
	sscanf(line.c_str(), "%lf %lf", &jd0, &gamma0);

	while (lambda < 0.)
		lambda += 360.;
	while (lambda >= 360.)
		lambda -= 360.;

	sprintf(buf, "lambda: %lf", lambda);
	spin_text.push_back(buf);

	sprintf(buf, "beta: %lf", beta);
	spin_text.push_back(buf);

	sprintf(buf, "gamma: %lf", gamma0);
	spin_text.push_back(buf);

	sprintf(buf, "period[h]: %lf", P);
	spin_text.push_back(buf);

	sprintf(buf, "jd_gamma0: %lf", jd0);
	spin_text.push_back(buf);


	merge(spin_text);

	f.close();
}

string Header::getHeaderString()
{
	ostringstream s;

	for (int i = 0; i < keys.size(); i++)
		if (keys[i].compare("date") == 0 )
			s << "# " << keys[i] << ": " << currentDateTime() << "\n";
		else
			s << "# " << keys[i] << ": " << values[i] << "\n";
	for (int i = 0; i < text.size(); i++)
		s << text[i] << "\n";

	return s.str();

}

const string Header::currentDateTime()
{
    time_t     now = time(0);
    struct tm  tstruct;
    char       buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%Y-%m-%d.%X", &tstruct);

    return buf;
}

