#include "Triangle.h"

Triangle::Triangle()
{
	// do nothihg
}


Triangle::Triangle(const Triangle& obj)
{
	/** copy construktor, tak na wszelki wypadek powinien ino być
	 * bo przy korzystaniu z vectora jest kopiowanie
	 */
// 	std::cout << "copy constructor at work\n";
	for (int i = 0; i < 3; i++)
	{
		vertices[i].x = obj.vertices[i].x;
		vertices[i].y = obj.vertices[i].y;
		vertices[i].z = obj.vertices[i].z;
		tex_coords[i].x = obj.tex_coords[i].x;
		tex_coords[i].y = obj.tex_coords[i].y;
	}
	b = obj.b;
	c = obj.c;
	xi = obj.xi;
	eta = obj.eta;
	loner_id = obj.loner_id;
}


bool Triangle::checkForPrimeMeridian()
{
	/** zwraca true jeżli trójkąt zawiera linię y=0,
	 * czyli południk zerowy
	 * WAŻNE: tutaj liczony jest parametr loner_id
	 */
	loner_id = -1;
	for (int i = 0; i < 3; i++)
	{
		/** dla każdego punktu sprawdzane jest, czy pozostałe
		 * punkty nie leżą po przeciwnej stronie lini y=0
		 * jeżeli tak => przechodzi przez równik zerowy
		 */


		if ( isNearZero(tex_coords[i].x ) )
		if ( isNearOne(tex_coords[(i+1) % 3].x))
		if ( isNearOne(tex_coords[(i+2) % 3].x))
		{
			loner_id = i;
			return true;
		}

		if ( isNearOne(tex_coords[i].x ) )
		if ( isNearZero(tex_coords[(i+1) % 3].x))
		if ( isNearZero(tex_coords[(i+2) % 3].x))
		{
			loner_id = i;
			return true;
		}
	}
	return false;
}


double Triangle::calculateXi()
{
	/** oblicza parametr xi
	 * który jest używany do wyliczenia współrzędnych
	 * punktów A' i A". Powstaje z warunku że y=0
	 */
	b = vertices[(loner_id + 1) % 3] - vertices[loner_id];
	b /= length(b);

	return - vertices[loner_id].y / b.y;
}

double Triangle::calculateEta()
{
	/** oblicza parametr eta
	 * który jest używany do wyliczenia współrzędnych
	 * punktów A' i A". Powstaje z warunku że y=0
	 */
	c = vertices[(loner_id + 2) % 3] - vertices[loner_id];
	c /= length(c);

	return - vertices[loner_id].y / c.y;
}

void Triangle::calculateBeta()
{
	for (int j = 0; j < 3; j++)
   	{
		dvec3 point = vertices[j];

		double r = length(point);
		double alpha = atan2(point.y, point.x);
		double beta = acos(point.z / r);

		alpha = 0.5 + alpha/2./M_PI;
		beta = 1. - beta/M_PI;

		tex_coords[j].y = beta;
	}
}

bool Triangle::isNearZero(double u)
{
	if (u >=0. && u < 0.3)
		return true;
	else
		return false;
}

bool Triangle::isNearOne(double u)
{
	if ( u <= 1. && u > 0.7)
		return true;
	else
		return false;
}

void Triangle::calculateNormal()
{
	dvec3 AB = vertices[1] - vertices[0];
	dvec3 AC = vertices[2] - vertices[0];

	normal = normalize( cross(AB, AC) );
}
