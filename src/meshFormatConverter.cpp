/***
 *                          _     _____                          _    ____                          _
 *      _ __ ___   ___  ___| |__ |  ___|__  _ __ _ __ ___   __ _| |_ / ___|___  _ ____   _____ _ __| |_ ___ _ __
 *     | '_ ` _ \ / _ \/ __| '_ \| |_ / _ \| '__| '_ ` _ \ / _` | __| |   / _ \| '_ \ \ / / _ \ '__| __/ _ \ '__|
 *     | | | | | |  __/\__ \ | | |  _| (_) | |  | | | | | | (_| | | | |__| (_) | | | \ V /  __/ |  | ||  __/ |
 *     |_| |_| |_|\___||___/_| |_|_|  \___/|_|  |_| |_| |_|\__,_|\_\ \____\___/|_| |_|\_/ \___|_|   \__\___|_|
 *
 */

#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include "Mesh.h"
#include "Options.h"

using namespace std;
using namespace glm;


int main(int argc, char *argv[])
{

	Options options(argc, argv);

	Mesh mesh(options.filename_in, options.in_format);
	mesh.print_tex_coords = options.print_tex_coords;
	mesh.arraysToTriangles();

	if (options.print_tex_coords)
		mesh.calculateTextureCoordinates();

	if (options.remove_inside)
		mesh.removeInside(options.GRID_DIM);

	if (options.center)
		mesh.center(options.GRID_DIM);

	// przejście z Triangles znowu na Arays i dopiero zapis do pliku
	// podczas przejscia usuwane sa powtarzajace sie trojkaty
 	mesh.trianglesToArrays();

	if (options.normalize_mesh)
		mesh.normalize();

	if (options.load_inp)
		mesh.header.loadInpFile(options.inp_filename);
	if (options.load_spin)
		mesh.header.loadSpinFile(options.spin_filename);
	if (options.random_info)
		mesh.header.randomInfo(options.grid, options.harmonics, options.seed);

	if (options.update_header)
		mesh.header.merge(options.header_items);

	if (strcmp(options.filename_out, "STDOUT") == 0)
		mesh.print(options.out_format);
	else
		mesh.save(options.filename_out, options.out_format);

	return 0;
}




