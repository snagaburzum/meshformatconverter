#ifndef OPTIONS_H_
#define OPTIONS_H_

#include <iostream>
#include <stdio.h>
#include <algorithm>
#include <string.h>
#include "Mesh.h"
#include <vector>

using namespace std;

class Options
{
public:
	Options();
	Options(int argc, char *argv[]);
	void printHelp();
	void parse(int argc, char *argv[]);
	vector<string> split(const string &text, char sep);

	const char *filename_in;
	const char *filename_out;

	int out_format;
	int in_format;
	int GRID_DIM;

	bool normalize_mesh, print_tex_coords;
	bool remove_inside, center;

	bool load_inp;
	bool load_spin;
	string inp_filename;
	string spin_filename;

	bool update_header;
	string header_raw_text;
	vector<string> header_items;

	bool random_info;
	string grid, harmonics, seed;
};

#endif /* OPTIONS_H_ */
