meshFormatConverter
==================

This program converts formats of mesh models.

Author: Grzegorz Dudzinski       g.p.dudzinski@gmail.com

Download
--------------------

- using [git](https://git-scm.com/): just run command

`git clone https://snagaburzum@bitbucket.org/snagaburzum/meshformatconverter.git`

- or go to https://bitbucket.org/snagaburzum/meshformatconverter/downloads and
	download reposotory. Then unzip and _voila_.

Install
-------------
Enter the meshformatconverter directory and run `make` command

Update
-------------
every time the software is updated, you have to update it locally as well.
using git, type `git pull https://snagaburzum@bitbucket.org/snagaburzum/meshformatconverter.git`
or download the repository again, replace the directory.

In both cases you have to recompile using `make`.


Features
-------------------
*TODO*




### Usage:
./meshFormatConverter input_filename output_filename [options]

 IF no file formats are specified, file extensions are parsed

example
./meshFormatConverter model.obj model.shp -if obj -of shp -n
#### Options:

	-if, --input_format <format>    file format for input file:
	                                obj - wavefront obj file
	                                |v .. .. ..                   |
	                                |f ../../.. ../../.. ../../.. |

	                                shp - wavefront shape
	                                |num_vertices num_indices     |
	                                |... ... ...                  |
	                                |... ... ...                  |


	-of, --output_format <format>   see --input_format

	-inp filename                   load information from .inp file to header
	-spin filename                  load information from SPIN DAMIT file to
	                                header
	-random_info <grid> <harmonics> <seed>      print grid subdivision depth
	                                <grid>, harmonics degree <harmonics> and
	                                random generator <seed> to the header

	-n, --normalize                 normalize input mesh
	-notex                          do not print texture coordinates
	-tex                            print texture coordinates
	-remove_inside GRID_DIM         remove triangles with angle between normal and
	                                direction to body center grater than 90deg
	                                GRID_DIM -- dimenstions used in generating the
	                                model, used for center computation
	-center GRID_DIM                move body to the center. GRID_DIM - grid size

	-update_header <string>         string=key:value,key2:value2,...


Header
-----------------

###Default comments (key-value pairs)
Every comment in a `.obj` file must beggin with `#`.

There are default key-value pairs defined and they will always appear at the beggining of the file, even if you delete them or create new `.obj` file from `.shp`.
Converting one `.obj` to another, e.g. applying size normalization, the values of the entries will be preserved.

The pairs structure is simple:

```
# key: value
```
Everything before `:` is a key, after `:` is value, so don't use `:` in the comments :)

Default values are

* `date: YYYY-MM-DD.HH:MM:SS`  it is generated automatically every time a file is created
* `target: Name of the target`    for example "433 Eros"
* `method: `  specifies inversion method used for creating the model, e.g "SAGE", or "convex inversion"
* `nr: `  wtf
* `period[h]: `  rotational period in hours
* `lambda: `
* `beta: `   $$\lambda$$ and $$\beta$$ of the pole orientation in ecliptic ref. frame
* `gamma: `  $$\gamma$$ angle of the asteroid that needs to be applied for the time specified by ''jd_gamma0''
* `jd_gamma0: ` time in which asteroid has rotational phase equal to ''gamma''
* `R_max: `  the length of the longest vector in the model

###Other comments
You can add any comments you like anywhere in the file by beggining a line with `#`. If your comment looks like key-valu pair
it will be also stored. All the commnets will appear in the header just after the default pairs, starting with added pairs and non key-value comments at the end.
